import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
from SubBack import SubWindow
import pandas as pd
    
class Mainwindow(QMainWindow):
    def Other(self):
        self.window=QtWidgets.QMainWindow()
        self.ui=SubWindow()
        self.ui.setupUi(self.window)
        self.window.show()
      
    def __init__(self):
        super(QMainWindow,self).__init__()
        loadUi("Front.ui",self) # Here we imported the QT Designer file which we made as Python GUI FIle.
        self.load_table()
        # To remove the default Windows Frame Design.
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        # Backgroud of Window transparent.
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        
        self.btnMini.clicked.connect(lambda: self.showMinimized())
        self.btnCross.clicked.connect(lambda: self.close())
        from SubBack import SubWindow
        self.btnAdd.clicked.connect(self.Other)

        
        
        # Function to load the previous data of student at the start of program.
    
        #Button Functions
        self.btnSubmit.clicked.connect(self.AddForm)
   
        
    Array=[]
    def load_table(self):
        a="ENTER DATA"
        self.txtName.setText(a)
        self.txtEmail.setText(a)
        self.txtCnic.setText(a)
        self.txtContact.setText(a)          
    def AddForm(self):
        NAME = self.txtName.text()
        CNIC = self.txtCnic.text()
        EMAIL = self.txtEmail.text()
        CONTACT = self.txtContact.text()
        CD=self.comboDistrict.currentText()
        CT=self.comboTehsil.currentText()
        CS=self.comboStamp.currentText()
        CDE=self.comboDeed.currentText()



        if(NAME!= "ENTER DATA"and self.Check_Cnic(CNIC)and CNIC!= "ENTER DATA" and EMAIL!= "ENTER DATA" and CONTACT!="ENTER DATA"):
            Array = [NAME,CNIC,EMAIL,CONTACT,CD,CT,CS,CDE]
            with open('Data.csv', 'a+',encoding="utf-8",newline="") as fileInput:
                writer = csv.writer(fileInput)
                writer.writerows([Array])
            self.resetValues()
            msg=QMessageBox()
            msg.setWindowTitle("--- ADD FORM ---")
            msg.setText("ADDED SUCCESSFULLY.")
            msg.setIcon(QMessageBox.Information)
            msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
            font=QtGui.QFont()
            font.setPointSize(12)
            font.setBold(True)
            font.setWeight(75)
            msg.setFont(font)
            msg.exec()

        else:
            msg=QMessageBox()
            msg.setWindowTitle("--- ADD FORM ---")
            msg.setText("THE FORM IS ALREADY ADDED WITH SAME CREDIENTIALS.")
            msg.setIcon(QMessageBox.Information)
            msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
            font=QtGui.QFont()
            font.setPointSize(12)
            font.setBold(True)
            font.setWeight(75)
            msg.setFont(font)
            msg.exec()
            self.resetValues()
    def resetValues(self):
        self.txtName.setText("ENTER DATA")
        self.txtCnic.setText("ENTER DATA")
        self.txtEmail.setText("ENTER DATA")
        self.txtContact.setText("ENTER DATA")
        
    def Check_Cnic(self,CNIC): 
        with open('Data.csv', 'r',encoding="utf-8",newline="") as fileInput:
            data = list(csv.reader(fileInput))
            for row in data:
                if CNIC == row[1]:
                    return False
        return True
    
    def Subwindow(self):
       msg=QMessageBox()
       msg.setWindowTitle("--- ADD PARTY---")
       msg.setText("ADDED  WITH THE INFORMATION GIVEN.")
       msg.setIcon(QMessageBox.Information)
       msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
       font=QtGui.QFont()
       font.setPointSize(12)
       font.setBold(True)
       font.setWeight(75)
       msg.setFont(font)
       msg.exec()


app = QApplication(sys.argv)
window = Mainwindow()
window.show()
sys.exit(app.exec_())
